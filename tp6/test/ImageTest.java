import org.junit.*;
import static org.junit.Assert.*;

//import jdk.jfr.Timestamp;
import image.Image;
import image.color.*;
import image.util.*;

public class ImageTest{
    @Test
    public void heightTest(){
    Image Im = new Image(100, 100);
    assertSame(100, Im.getHeight());
    }

    @Test
    public void widthTest(){
    Image Im = new Image(100, 100);
    assertSame(100, Im.getWidth());
    }

    @Test 
    public void getPixelTest(){
    Image Im = new Image(100, 100);
    assertSame(Im.getPixel(2,6), Im.getPixel(2,6));
    }

    // ---Pour permettre l'exécution des test----------------------
    public static junit.framework.Test suite() {
        return new junit.framework.JUnit4TestAdapter(ImageTest.class);
    }
}