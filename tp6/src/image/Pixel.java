package image;

import image.color.*;

public class Pixel {
    private GrayColor color;

    public Pixel(GrayColor c){
        this.color = c;
    }

    /** change la couleur du pixel
	 * @param C pixel color
	 */
    public void setColor(GrayColor C){
        this.color = C;
    }

    /** get pixel's color.	 
	 * @return pixel's color.
	 */
    public GrayColor getColor(){
        return this.color;
    }

    /** give the gray level difference between 2 pixels
	 * @param P the other pixel
	 * @return Math.abs(this.color.getGrayLevel() - p.getColor().getGrayLevel()); difference between 2 pixel
	 */
    public int colorLevelDifference(Pixel p){
        return Math.abs(this.color.getGrayLevel() - p.getColor().getGrayLevel());
    }

    public boolean equals(Pixel o){
        if(o instanceof Pixel){
            Pixel other = (Pixel) o;
            return (this.color == other.getColor());
        }
        else return false;
    }
}
