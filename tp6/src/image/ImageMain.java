package image;

import image.color.*;
import image.util.*;


public class ImageMain {

    public static void main(String[] args) {
        Image I = new Image(200,150);

        I.fillRectangle(20, 30, 30, 50, GrayColor.BLACK);
        I.fillRectangle(50, 100, 40, 40, new GrayColor(128));
        I.fillRectangle(90, 20, 70, 50, new GrayColor(200));   
            
        ImageDisplayer displayer = new ImageDisplayer();
        displayer.display(I, "Image", 300, 500);


        Image Img = ImageLoader.loadPGM(args[0]);
        displayer.display(Img, "Normal", 100, 100);
        displayer.display(Img.negative(), "Nega", 400, 100);
        displayer.display(Img.edgeExtraction(Integer.parseInt(args[1])), "EdgeExtr", 400, 100);
        displayer.display(Img.decreaseNbGrayLevels(Integer.parseInt(args[2])), "DecreaseGray", 400, 100);

    }
}
