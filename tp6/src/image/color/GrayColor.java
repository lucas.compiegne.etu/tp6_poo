package image.color;

public class GrayColor{
    public static GrayColor WHITE = new GrayColor(255);
    public static GrayColor BLACK = new GrayColor(0);
    
    private int grayLevel;
    private double alpha;

    public GrayColor(int level){
        this.alpha = 1;
        this.grayLevel = level;
    }

    /** return gray level
	 * @return gray level
	 */
    public int getGrayLevel(){
        return this.grayLevel;
    }

    /** return alpha
	 * @return Alpha
	 */
    public double getAlpha(){
        return this.alpha;
    }

    /** modifie alpha
	 * @param a new alpha
	 */
    public void setAlpha(double a){
        this.alpha = a;
    }

    public boolean equals(GrayColor o){
        if(o instanceof GrayColor){
            GrayColor other = (GrayColor) o;
            return (this.alpha == other.getAlpha()) && (this.grayLevel == other.getGrayLevel());
        }
        else return false;
    }
}