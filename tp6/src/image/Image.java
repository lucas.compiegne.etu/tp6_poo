package image;

import image.color.*;

public class Image implements ImageInterface{
    private int Width;
    private int Height;
    private Pixel Tab[][];

    public Image(int l, int L){
        this.Width = l;
        this.Height = L;
        this.Tab = new Pixel[l][L];

        for(int i = 0; i < l; i++){
            for(int j = 0; j < L; j++){
                Tab[i][j] = new Pixel(GrayColor.WHITE);
            }
        }
    }
    /** returns width of the image
	 * @return width of the image
	 */
    public int getWidth() {
        return this.Width;
    }

    /** returns height of the image
	 * @return height of the image
	 */
    public int getHeight() {
        return this.Height;
    }

    /** get the pixel at coord (x,y) of this image. (0,0) is top left corner pixel.
	 * @param x the horizontal coordinate
	 * @param y the vertical coordinate
	 * @return pixel at coord (x,y) of this image. (0,0) is top left corner pixel.
	 * @exception UnknownPixelException if coord (x,y) is not valid for this image
	 */
    public Pixel getPixel(int x, int y) throws UnknownPixelException {
        return this.Tab[x][y];
    }

    /** change the pixel's color at coord (x,y) of this image. (0,0) is top left corner pixel.
	 * @param x the horizontal coordinate
	 * @param y the vertical coordinate
     * @param color pixel color
	 */
    public void changeColorPixel(int x, int y, GrayColor color) throws UnknownPixelException{
        this.Tab[x][y].setColor(color);
    }

    /** create rectangle that start at coord (x,y) of this image.
	 * @param x the horizontal coordinate
	 * @param y the vertical coordinate
     * @param width width of the rectangle
     * @param height height of the rectangle
     * @param color color of the new pixel
	 */
    public void fillRectangle(int x, int y, int width, int height, GrayColor color){
        for(int i = x; i < x+width; i++){
            for(int j = y; j < y+height; j++){
                this.Tab[i][j] = new Pixel(color);
            }
        }
    }

    /** affiche l'image négative
     * @return image result
	 */
    public Image negative(){
        Image Result = new Image(this.getWidth(), this.getHeight());
        for(int i = 0; i < this.getWidth(); i++){
            for(int j = 0; j < this.getHeight(); j++){
                int a = 255 - this.Tab[i][j].getColor().getGrayLevel();
                Result.changeColorPixel(i, j, new GrayColor(a));
            }
        }
        return Result;
    }

    /** affiche les bords de l'image
	 * @param threshold modifie la bordure
	 * @return result image
	 */
    public Image edgeExtraction(int threshold){
        Image Result = new Image(this.getWidth(), this.getHeight());
        for(int i = 0; i < this.getWidth(); i++){
            for(int j = 0; i < this.getHeight(); j++){
                if((this.Tab[i-1][j].colorLevelDifference(this.Tab[i][j]) > threshold) && (this.Tab[i][j-1].colorLevelDifference(this.Tab[i][j]) > threshold)){
                    Result.changeColorPixel(i, j, GrayColor.BLACK);
                }
            }
        }
        return Result;
    }

    /** decrease the number of gray levels of the picture
	 * @param nbGrayLevels 
     * @return result image
	 */
    public Image decreaseNbGrayLevels(int nbGrayLevels){
        Image Result = new Image(this.getWidth(), this.getHeight());
        int t = 256/nbGrayLevels;
        for(int i = 0; i < this.getWidth(); i++){
            for(int j = 0; i < this.getHeight(); j++){
                int l = this.Tab[i][j].getColor().getGrayLevel();
                Result.changeColorPixel(i, j, new GrayColor((l/t)*t));
            }
        }
        return Result;
    }
}
